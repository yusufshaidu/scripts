import numpy as np
import json
import os
import argparse
from ase import Atom

def atomic_number(symbol):
    string_symbol = {'H': 1, 'He': 2, 'Li': 3, 'Be': 4, 'B': 5, 'C': 6, 'N': 7, 'O': 8, 'F': 9, 'Ne': 10, 'Na': 11, 'Mg': 12, 'Al': 13, 'Si': 14, 'P': 15, 'S': 16, 'Cl': 17, 'K': 19, 'Ar': 18, 'Ca': 20, 'Sc': 21, 'Ti': 22, 'V': 23, 'Cr': 24, 'Mn': 25, 'Fe': 26, 'Ni': 28, 'Co': 27, 'Cu': 29, 'Zn': 30, 'Ga': 31, 'Ge': 32, 'As': 33, 'Se': 34, 'Br': 35, 'Kr': 36, 'Rb': 37, 'Sr': 38, 'Y': 39, 'Zr': 40, 'Nb': 41, 'Mo': 42, 'Tc': 43, 'Ru': 44, 'Rh': 45, 'Pd': 46, 'Ag': 47, 'Cd': 48, 'In': 49, 'Sn': 50, 'Sb': 51, 'I': 53, 'Te': 52, 'Xe': 54, 'Cs': 55, 'Ba': 56, 'La': 57, 'Ce': 58, 'Pr': 59, 'Nd': 60, 'Pm': 61, 'Sm': 62, 'Eu': 63, 'Gd': 64, 'Tb': 65, 'Dy': 66, 'Ho': 67, 'Er': 68, 'Tm': 69, 'Yb': 70, 'Lu': 71, 'Hf': 72, 'Ta': 73, 'W': 74, 'Re': 75, 'Os': 76, 'Ir': 77, 'Pt': 78, 'Au': 79, 'Hg': 80, 'Tl': 81, 'Pb': 82, 'Bi': 83, 'Po': 84, 'At': 85, 'Rn': 86, 'Fr': 87, 'Ra': 88, 'Ac': 89, 'Pa': 91, 'Th': 90, 'Np': 93, 'U': 92, 'Am': 95, 'Pu': 94, 'Cm': 96, 'Bk': 97, 'Cf': 98, 'Es': 99, 'Fm': 100, 'Md': 101, 'No': 102, 'Rf': 104, 'Lr': 103, 'Db': 105, 'Bh': 107, 'Sg': 106, 'Mt': 109, 'Rg': 111, 'Hs': 108}
 
    return  string_symbol[symbol]
def atomic_mass(symbol):
    string_symbol={'H': 1.0079, 'He': 4.0026, 'Li': 6.941, 'Be': 9.0122, 'B': 10.811, 'C': 12.0107, 'N': 14.0067, 'O': 15.9994, 'F': 18.9984, 'Ne': 20.1797, 'Na': 22.9897, 'Mg': 24.305, 'Al': 26.9815, 'Si': 28.0855, 'P': 30.9738, 'S': 32.065, 'Cl': 35.453, 'K': 39.0983, 'Ar': 39.948, 'Ca': 40.078, 'Sc': 44.9559, 'Ti': 47.867, 'V': 50.9415, 'Cr': 51.9961, 'Mn': 54.938, 'Fe': 55.845, 'Ni': 58.6934, 'Co': 58.9332, 'Cu': 63.546, 'Zn': 65.39, 'Ga': 69.723, 'Ge': 72.64, 'As': 74.9216, 'Se': 78.96, 'Br': 79.904, 'Kr': 83.8, 'Rb': 85.4678, 'Sr': 87.62, 'Y': 88.9059, 'Zr': 91.224, 'Nb': 92.9064, 'Mo': 95.94, 'Tc': 98.0, 'Ru': 101.07, 'Rh': 102.9055, 'Pd': 106.42, 'Ag': 107.8682, 'Cd': 112.411, 'In': 114.818, 'Sn': 118.71, 'Sb': 121.76, 'I': 126.9045, 'Te': 127.6, 'Xe': 131.293, 'Cs': 132.9055, 'Ba': 137.327, 'La': 138.9055, 'Ce': 140.116, 'Pr': 140.9077, 'Nd': 144.24, 'Pm': 145.0, 'Sm': 150.36, 'Eu': 151.964, 'Gd': 157.25, 'Tb': 158.9253, 'Dy': 162.5, 'Ho': 164.9303, 'Er': 167.259, 'Tm': 168.9342, 'Yb': 173.04, 'Lu': 174.967, 'Hf': 178.49, 'Ta': 180.9479, 'W': 183.84, 'Re': 186.207, 'Os': 190.23, 'Ir': 192.217, 'Pt': 195.078, 'Au': 196.9665, 'Hg': 200.59, 'Tl': 204.3833, 'Pb': 207.2, 'Bi': 208.9804, 'Po': 209.0, 'At': 210.0, 'Rn': 222.0, 'Fr': 223.0, 'Ra': 226.0, 'Ac': 227.0, 'Pa': 231.0359, 'Th': 232.0381, 'Np': 237.0, 'U': 238.0289, 'Am': 243.0, 'Pu': 244.0, 'Cm': 247.0, 'Bk': 247.0, 'Cf': 251.0, 'Es': 252.0, 'Fm': 257.0, 'Md': 258.0, 'No': 259.0, 'Rf': 261.0, 'Lr': 262.0, 'Db': 262.0, 'Bh': 264.0, 'Sg': 266.0, 'Mt': 268.0, 'Rg': 272.0, 'Hs': 277.0}

    return  string_symbol[symbol]

def convert_latt_to_lammps (a):
    '''
        This procedure is defined in lammps documentation

        a: matrix to be converted

        return: converted matrix in [a1, a2, a3]

    '''
    a = np.asarray(a)

    lat_vec = np.zeros((3,3), dtype = float)

    lat_vec[0] = [np.linalg.norm(a[0]), 0., 0.]

    unit_a0 = a[0]/np.linalg.norm(a[0])
    lat_vec[1] = [np.dot(a[1], unit_a0), np.linalg.norm(np.cross(unit_a0, a[1])), 0.]
    
    lat_vec[2][0] = np.dot(a[2],unit_a0)

    lat_vec[2][1] = (np.dot(a[1], a[2]) - lat_vec[1][0] * lat_vec[2][0]) / lat_vec[1][1]
    lat_vec[2][2] = np.sqrt(np.linalg.norm(a[2])**2 - lat_vec[2][0]**2 - lat_vec[2][1]**2)

    return lat_vec
def convert_vectors_to_lammps_vectors(lat_vec_o, x):
    '''
        This procedure is defined in lammps documentation
        x: vector to be converted
        lat_vec_o: the original lattice vectors
        return: convected vector
    '''
    lat_vec_o = np.asarray(lat_vec_o)

    vect = convert_latt_to_lammps (lat_vec_o)
    
    vol = np.abs(np.dot(lat_vec_o[0], np.cross(lat_vec_o[1], lat_vec_o[2])))

    b1 = np.cross(lat_vec_o[1], lat_vec_o[2])/vol
    b2 = np.cross(lat_vec_o[2], lat_vec_o[0])/vol
    b3 = np.cross(lat_vec_o[0], lat_vec_o[1])/vol

    return np.dot(np.dot(np.transpose(vect), [b1,b2,b3]), x)

def jsonxyz2lammp(indir, outdir):


    if not os.path.exists(outdir):
        os.mkdir(outdir)


     
    files = []
    en = []
    y = [os.path.join(indir,x) for x in os.listdir(indir)]

    for fil in y:
        key = fil.split('/')[-1].split('.')[0]

        if not os.path.exists(os.path.join(outdir,key[0])):
            os.mkdir(os.path.join(outdir,key[0]))

        dfout=open(os.path.join(outdir,key[0],key+".pos"),"w")

        
        df = open(fil)
        data = json.load(df)
        dfout.write('{} {}\n'.format('#  ',key))
        dfout.write("\n")
        Natoms = len(data['atoms'])
        #print(key[:3],data['energy'][0]*13.6056980659)

        ntype = 0
        kinds = []
        for atom in data['atoms']:
            idx, kind, position, force = atom
            if kind not in kinds:
                ntype += 1
                kinds.append(kind)
        kind_dic = {}
        for kd in range(1,len(kinds)+1):
            kind_dic[kinds[kd-1]] = kd

        dfout.write('{} {}\n'.format(Natoms,'atoms'))
        dfout.write('{} {}\n'.format(ntype,'atom types'))
        dfout.write("\n")

        lat_vec = np.asarray(data['lattice_vectors']).astype(float)
        if data['unit_of_length'] in ['bohr','au','BOHR','Bohr']:
            lat_vec = lat_vec*0.529177

#        print('original',lat_vec)
        
        # convert input to lammps basis
        lat_vec = convert_latt_to_lammps (lat_vec)

 #       print('transformed',lat_vec)

        xl = 0.
        xh = lat_vec[0][0] + 1e-5
        yl = 0.
        yh = lat_vec[1][1] + 1e-5
        xy = lat_vec[1][0]
        zl = 0.
        zh = lat_vec[2][2] + 1e-5
        xz = lat_vec[2][0]
        yz = lat_vec[2][1]
         
        dfout.write('{} {} {} {}\n'.format(xl,xh,'xlo','xhi'))
        dfout.write('{} {} {} {}\n'.format(yl,yh,'ylo', 'yhi'))
        dfout.write('{} {} {} {}\n'.format(zl,zh,'zlo', 'zhi'))
        dfout.write('{} {} {} {}\n'.format(xy,xz,yz , 'xy xz yz'))
        dfout.write("\n")
        dfout.write("Masses\n")
        dfout.write("\n")
        for i in range(ntype):
            idx = kind_dic[kinds[i]]
            #mass = atomic_mass(kinds[i])
            mass = Atom(kinds[i]).mass
            dfout.write('{} {}\n'.format(idx, mass))

        dfout.write("\n")
        dfout.write("Atoms\n")
        dfout.write("\n")
       
        #recall lattice vectors
        lat_vec = np.asarray(data['lattice_vectors']).astype(float)
        if data['unit_of_length'] in ['bohr','au','BOHR','Bohr']:
            lat_vec = lat_vec*0.529177

        for atom in data['atoms']:
            idx, kind, position, force = atom

            if data['atomic_position_unit']=='crystal':
                position=np.dot(np.transpose(lat_vec), np.asarray(position).astype(float))

            if data['atomic_position_unit']=='cartesian':
                if data['unit_of_length'] in ['bohr','au','BOHR','Bohr']:
                    position = np.asarray(position)*0.529177

            position = convert_vectors_to_lammps_vectors(lat_vec, position)

            xyz = '{}  {}  {}'.format(idx, kind_dic[kind], ' '.join(map(str, position)) )
            dfout.write('{}\n'.format(xyz))
 

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Making lammps config from json')
    parser.add_argument('-i', '--indir', type=str,
                        help='in path', required=True)
    parser.add_argument('-o', '--outdir', type=str,
                        help='out path', required=True)
    args = parser.parse_args()
    if args.indir and args.outdir:
       jsonxyz2lammp(indir=args.indir, outdir=args.outdir)
