
python3 generate_3D_supercell_stages.py  -a 2.47 -c 3.65 -o stages/ -typea1 root3 -typea2 root3 -Na1 1 -Na2 1 -seq A -Li_seq 0
python3 generate_3D_supercell_stages.py  -a 2.47 -c 3.49 -o stages/ -typea1 root3 -typea2 root3 -Na1 1 -Na2 1 -seq AA -Li_seq 0
python3 generate_3D_supercell_stages.py  -a 2.47 -c 3.46 -o stages/ -typea1 root3 -typea2 root3 -Na1 1 -Na2 1 -seq AAB -Li_seq 1
python3 generate_3D_supercell_stages.py  -a 2.47 -c 3.44 -o stages/ -typea1 root3 -typea2 root3 -Na1 1 -Na2 1 -seq AABABBAB -Li_seq 1,5
python3 generate_3D_supercell_stages.py  -a 2.47 -c 3.44 -o stages/ -typea1 root3 -typea2 root3 -Na1 1 -Na2 1 -seq AABAB -Li_seq 1
python3 generate_3D_supercell_stages.py  -a 2.47 -c 3.53 -o stages/ -typea1 normal -typea2 root3 -Na1 2 -Na2 1 -seq AA -Li_seq 1
python3 generate_3D_supercell_stages.py  -a 2.47 -c 3.53 -o stages/ -typea1 normal -typea2 normal -Na1 2 -Na2 2 -seq AA -Li_seq 1
python3 generate_3D_supercell_stages.py  -a 2.47 -c 3.53 -o stages/ -typea1 normal -typea2 normal -Na1 2 -Na2 2 -seq AABB -Li_seq 1,3

python3 generate_3D_supercell_stages.py  -a 2.47 -c 3.54 -o stages/ -typea1 root3 -typea2 root7 -Na1 1 -Na2 1 -seq AA -Li_seq 1
python3 generate_3D_supercell_stages.py  -a 2.47 -c 3.51 -o stages/ -typea1 root3 -typea2 root7 -Na1 1 -Na2 1 -seq AABB -Li_seq 1,3

python3 generate_3D_supercell_stages.py  -a 2.47 -c 3.51 -o stages/ -typea1 normal -typea2 root7 -Na1 2 -Na2 1 -seq AABB -Li_seq 1,3
python3 generate_3D_supercell_stages.py  -a 2.47 -c 3.48 -o stages/ -typea1 root7 -typea2 root7 -Na1 1 -Na2 1 -seq AABB -Li_seq 1,3

python3 generate_3D_supercell_stages.py  -a 2.47 -c 3.50 -o stages/ -typea1 normal -typea2 root7 -Na1 3 -Na2 1 -seq AABB -Li_seq 1,3

python3 generate_3D_supercell_stages.py  -a 2.47 -c 3.48 -o stages/ -typea1 normal -typea2 root7 -Na1 4 -Na2 1 -seq AABB -Li_seq 1,3
