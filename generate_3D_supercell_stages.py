import itertools as it
import numpy as np
import json, os
import re, itertools
from string import ascii_letters, punctuation, digits
from random import choice, randint
import argparse

def basic_lattice(a0,c0, trans_vect):
    v01=np.asarray([0.5,0.5/np.sqrt(3.0),0.0])*a0 + trans_vect
    v02=np.asarray([0,1.0/np.sqrt(3.0),0.0])*a0 + trans_vect
    Ndim1=10
    Ndim2=10
    Ndim3=1
    a1=np.asarray([1.,0.,0.])*a0
    a2=np.asarray([-0.50,np.sqrt(3.0)/2.,0])*a0
    a3=np.asarray([0.,0.,c0])
    v=np.asarray([v01,v02]).astype(float)
    v_repeat=[]
    for i,j, k in itertools.product(range(-Ndim1, Ndim1+1),range(-Ndim2, Ndim2+1), range(-Ndim3, Ndim3+1)):
        for single_v in v:
            v_repeat.append(single_v + i*a1 + j*a2 + k*a3)

    #print(v_repeat)
    return v_repeat

def generate_graphite(a0,c0,
                      Na1=1,Na2=1,Na3=1,
                      typea1='normal',
                      typea2='normal',
                      translate=False):
    '''
        a0,c0: the lattice vectors
        Na1 and Na2 are the number of cell units
        typea1 : type of lattice vector of a1 eg normal, root3 and root7
        typea2 : type of lattice vector of a1 eg normal, root3 and root7
        rtheta: rotation angle of the layer
    '''
    if typea1=='normal' and typea2=='normal':
        a1 = Na1*np.asarray([0.5,np.sqrt(3.0)/2.,0.])*a0
        a2 = Na2*np.asarray([-0.50,np.sqrt(3.0)/2.,0])*a0
        a3 = np.asarray([0.,0.,c0])
    if typea1=='normal' and typea2=='root3':
        a1 = Na1*np.asarray([0.5,np.sqrt(3.0)/2.,0.])*a0
        a2 = Na2*np.asarray([-1.50,np.sqrt(3.0)/2.,0])*a0
        a3 = np.asarray([0.,0.,c0])
    if typea1=='normal' and typea2=='root3':
        a1 = Na1*np.asarray([0.5,np.sqrt(3.0)/2.,0.])*a0
        a2 = Na2*np.asarray([-1.50,np.sqrt(3.0)/2.,0])*a0
        a3 = np.asarray([0.,0.,c0])
    if typea1=='normal' and typea2=='root7':
        a1 = Na1*np.asarray([0.5,np.sqrt(3.0)/2.,0.])*a0
        a2 = Na2*np.asarray([-2.0,np.sqrt(3.0),0])*a0
        a3 = np.asarray([0.,0.,c0])
    if typea1=='root3' and typea2=='root3':
        a1 = Na1*np.asarray([1.5,np.sqrt(3.0)/2,0.])*a0
        a2 = Na2*np.asarray([-1.50,np.sqrt(3.0)/2.,0])*a0
        a3 = np.asarray([0.,0.,c0])
    if typea1=='root3' and typea2=='root7':
        a1 = Na1*np.asarray([1.5,np.sqrt(3.0)/2,0.])*a0
        a2 = Na2*np.asarray([-2.0,np.sqrt(3.0),0])*a0
        a3 = np.asarray([0.,0.,c0])
    if typea1=='root7' and typea2=='root7':
        a1 = Na1*np.asarray([2.0,np.sqrt(3.0),0.])*a0
        a2 = Na2*np.asarray([-2.0,np.sqrt(3.0),0])*a0
        a3 = np.asarray([0.,0.,c0])



    lat_vect = np.asarray([a1,a2,a3])
    lat_vect_T = lat_vect.T
    lat_vect_T_inv = np.linalg.inv(lat_vect_T)

    i=0
    ##define a translation vector

    if translate:
        trans_vect = np.asarray([0,2*a0/np.sqrt(3.),0])
    else:
        trans_vect = np.zeros(3)

    candidate = []
    for v in basic_lattice(a0,c0, trans_vect):
        pos =  np.dot(lat_vect_T_inv, v)
        count = 0
        for p in pos:

            if 0 <= p < 0.99999:
                #if p==1:
                #    print('What are you doing here')
                count += 1
        if count == 3:
            i+=1
            candidate.append(v)
            #print("{} {:.3f} {:.3f} {:.3f}".format(i, pos[0],pos[1],pos[2] ))

    return candidate, lat_vect




def main(a0,c0,
        Na1=1,Na2=1,
        typea1='normal',#type of lattice 1
        typea2='normal', #type of lattice 2
        sequence='A', # stages
        Li_seq='0,',
        outdir='./',
        adatom='Li'):


    _Li_seq = np.array(Li_seq.split(','), dtype=np.int32)
    Na3=len(sequence)
    print(Na3)
    Lipos = [[0, 0, 2*x*0.5*c0] for x in _Li_seq]

    filename = "{}x{}_supercell_{}x{}_stage_{}.vasp".format(typea1,typea2,Na1,Na2, sequence)

    shift = np.asarray([0,0,0.5/len(sequence)*Na3*c0])
    translate=False
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    coords, lattice_vect = generate_graphite(a0,c0,
                      Na1=Na1,Na2=Na2,Na3=1,
                      typea1=typea1,
                      typea2=typea2,
                      translate=translate)
    coords += shift
    nat = len(coords)


    #print(coords)
    if len(sequence)>1:
        for i, s in enumerate(sequence):
            if i == 0:
                continue
            translate=False
            if s=='B':
                translate = True
                
            shift = np.asarray([0,0,(2*i + 1)/len(sequence)*0.5 * Na3*c0])
            _coords, lattice_vect = generate_graphite(a0,c0,
                      Na1=Na1,Na2=Na2,Na3=1,
                      typea1=typea1,
                      typea2=typea2,
                      translate=translate)
            coords=np.append(coords, _coords + shift)
            nat += len(_coords)
    coords =  np.reshape(coords, (nat,3))
    symbols = ['C'] * nat
    
    trans_vect = np.array([0,2*a0/np.sqrt(3.),0])
    Li_positions = []
    Li_symbols = []
    for li_seq, Lip in zip(_Li_seq, Lipos):
        if sequence[li_seq] == 'B':
            #for i in range(3):
            Lip += trans_vect
            Li_positions.append(Lip)
            Li_symbols.append(adatom)
        else:
            Li_positions.append(Lip)
            Li_symbols.append(adatom)

    coords = np.append(coords, Li_positions)
    symbols.extend(Li_symbols)
    lattice_vect[2] *= Na3
    from ase import Atoms, io
    atoms = Atoms(positions=np.reshape(coords, [-1,3]), symbols=symbols, cell=lattice_vect, pbc=True)
    io.write(f'{outdir}/{filename}', atoms, sort=True,direct=True)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Making axsf file from json')
    parser.add_argument('-o', '--outdir', type=str,
                        help='out path', required=False, default='tmp')
    parser.add_argument('-a', '--a0', type=float,
                        help='alat', required=True)
    parser.add_argument('-c', '--c0', type=float,
                        help='calat', required=True)
    parser.add_argument('-Na1', '--Na1', type=int,
                        help='a1 units', required=False, default=1)
    parser.add_argument('-Na2', '--Na2', type=int,
                        help='a2 units', required=False, default=1)
    parser.add_argument('-Li_seq', '--Li_seq', type=str,
                        help='the sequence of adatom', required=False, default='0')
    parser.add_argument('-typea1', '--typea1', type=str,
                        help='typea1', required=False, default='normal')
    parser.add_argument('-typea2', '--typea2', type=str,
                        help='typea2', required=False, default='normal')
    parser.add_argument('-seq', '--sequence', type=str,
                        help='sequence', required=False, default='A')
    parser.add_argument('-adatom', '--adatom', type=str,
                        help='the chemical symbol of the adatom', required=False, default='Li')
    args = parser.parse_args()
    main(args.a0,args.c0,
        Na1=args.Na1,Na2=args.Na2,
        Li_seq=args.Li_seq,
        typea1=args.typea1,
        typea2=args.typea2,
        sequence=args.sequence,
        outdir=args.outdir,
        adatom=args.adatom)
